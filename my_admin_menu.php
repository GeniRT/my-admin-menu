<?php
/**
 * Plugin Name: My Admin Menu
 * Plugin URI: http://athtech.gr
 * Description: Testing the admin panel properties
 * Version: 1.0.0
 * Author: Jurgen Mone
 * Author URI: http://athtech.gr
 * License: GPL2
 */
/**
 *############################################################################################################################################*/

add_action('admin_menu', 'my_admin_menu');

function my_admin_menu()
{
    add_menu_page('My Top Level Menu Example', 'Available Tickets', 'manage_options', 'myplugin/myplugin-admin-page.php', 'myplguin_admin_page', 'dashicons-tickets', 6);
    add_submenu_page('myplugin/myplugin-admin-page.php', 'My Sub Level Menu Example', 'School List', 'manage_options', 'myplugin/myplugin-admin-sub-page.php', 'myplguin_admin_sub_page');
}

function array_flatten($array)
{
    if (!is_array($array)) {
        return FALSE;
    }
    $result = array();
    foreach ($array as $key => $value) {
        if (is_array($value)) {
            $result = array_merge($result, array_flatten($value));
        } else {
            $result[$key] = $value;
        }
    }
    return $result;
}

function myplguin_admin_sub_page()
{
    ?>
    <div class="wrap">
        <form id="movies-filter" method="get">
            <table class="widefat fixed" cellspacing="0">
                <thead>
                <tr>
                    <th id="cb" class="manage-column column-cb check-column" scope="col"><input type="checkbox"/></th>
                    <th id="columnname" class="manage-column column-columnname" scope="col">Name</th>
                    <th id="columnname" class="manage-column column-columnname" scope="col">Address</th>
                    <th id="columnname" class="manage-column column-columnname" scope="col">TK</th>
                    <th id="columnname" class="manage-column column-columnname" scope="col">Telephone</th>
                    <th id="columnname" class="manage-column column-columnname" scope="col">District</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th id="cb" class="manage-column column-cb check-column" scope="col"><input type="checkbox"/></th>
                    <th id="columnname" class="manage-column column-columnname" scope="col">Name</th>
                    <th id="columnname" class="manage-column column-columnname" scope="col">Address</th>
                    <th id="columnname" class="manage-column column-columnname" scope="col">TK</th>
                    <th id="columnname" class="manage-column column-columnname" scope="col">Telephone</th>
                    <th id="columnname" class="manage-column column-columnname" scope="col">District</th>
                </tr>
                </tfoot>
                <?php
                global $wpdb;

                $schools = $wpdb->get_results("SELECT Dytikou_tomea_athinwn, Voriou_tomea_athinwn, Kentrikos_tomea_athinwn, Notio_tomea_athinwn, Anatoliko_tomea_athinwn, Dytiki_attiki, Pirea_niswn, Dimos_thessalonikis, Dimos_kalamarias, Dimos_pylaia_xortiatis, Dimos_thermaikou, Dimos_thermis, Eidikis_agwgis FROM savecontactform7_5", ARRAY_N);

                $schoolArray = array();

                foreach ($schools as $value) {
                    // add each row returned into an array
                    $schoolArray[] = $value;
                }

                $arr = array_flatten($schoolArray);
                $arr = array_filter($arr);
                $iZero = array_values($arr);


                $pagenum = filter_input(INPUT_GET, 'pagenum') ? absint(filter_input(INPUT_GET, 'pagenum')) : 1;
                $limit = 20; // number of rows in page
                $offset = ($pagenum - 1) * $limit;
                $total = $wpdb->get_var("SELECT COUNT(id) FROM schools");
                $num_of_pages = ceil($total / $limit);

                $result = $wpdb->get_results("SELECT * FROM schools LIMIT $offset, $limit");
                foreach ($result as $print) {

                    $sampleArray[] = $print->name;
                    ?>
                    <tbody>
                    <tr <?php if (in_array($print->name, $iZero)): ?>
                        style="background:#F78181;"
                    <?php endif; ?>>
                        <th class="check-column" scope="row"><input type="checkbox"/></th>
                        <td class="column-columnname"><?php echo $print->name; ?> </td>
                        <td class="column-columnname"><?php echo $print->address; ?></td>
                        <td class="column-columnname"><?php echo $print->tk; ?></td>
                        <td class="column-columnname"><?php echo $print->telefone; ?></td>
                        <td class="column-columnname"><?php echo $print->district; ?></td>
                    </tr>
                    </tbody>
                <?php } ?>
            </table>
            <?php
            $page_links = paginate_links(array(
                'base' => add_query_arg('pagenum', '%#%'),
                'format' => '',
                'prev_text' => __('&laquo;', 'text-domain'),
                'next_text' => __('&raquo;', 'text-domain'),
                'total' => $num_of_pages,
                'current' => $pagenum
            ));

            if ($page_links) {
                echo '<div class="tablenav"><div class="tablenav-pages" style="margin: 1em 0">' . $page_links . '</div></div>';
            }
            //echo count($schoolArray); // show all array data

            //var_dump($iZero);
            //echo "<br />";
            //$arrsample = array_filter($sampleArray);
            //echo "<br />";
            //var_dump($arrsample);
            //echo "<br />";
            //$str1 = "6/θ Πειραματικό  ΔΣ ΑΠΘ (μη ενταγμένο)";
            //$str2 = "6/θ Πειραματικό  ΔΣ ΑΠΘ (μη ενταγμένο)";
            //$a = html_entity_decode('Waar    zijn mijn centjes&#x1f47c;');
            //var_dump(htmlspecialchars($str1));
            //echo "<br />";
            //var_dump(htmlspecialchars($str2));
            //echo "<br />";
            //var_dump(bin2hex($str1));
            //echo "<br />";
            //var_dump(bin2hex($str2));

            ?>

        </form>
    </div>
    <?php
}

function myplguin_admin_page()
{
    ?>
    <div class="wrap">
        <h2>Ticket Counter</h2>
        <form id="movies-filter" method="get">
            <table class="widefat fixed" cellspacing="0">
                <thead>
                <tr>
                    <th id="cb" class="manage-column column-cb check-column" scope="col"><input type="checkbox"/></th>
                    <th id="columnname" class="manage-column column-columnname" scope="col">Name</th>
                    <th id="columnname" class="manage-column column-columnname" scope="col">Count</th>
                    <th id="columnname" class="manage-column column-columnname" scope="col">Sold</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th id="cb" class="manage-column column-cb check-column" scope="col"><input type="checkbox"/></th>
                    <th id="columnname" class="manage-column column-columnname" scope="col">Name</th>
                    <th id="columnname" class="manage-column column-columnname" scope="col">Count</th>
                    <th id="columnname" class="manage-column column-columnname" scope="col">Sold</th>
                </tr>
                </tfoot>
                <?php
                global $wpdb;
                /*  $result = $wpdb->get_results ("SELECT
                                  wbk_services.name,
                                  count(wbk_appointments.service_id) as total,
                                  sum(wbk_appointments.quantity) as sold
                                  FROM wbk_services LEFT JOIN wbk_appointments ON
                                  wbk_services.id = wbk_appointments.service_id GROUP BY wbk_services.name"); */
                $result = $wpdb->get_results("
            SELECT
            Ekpaideutiko_programma as name,
            COUNT(id) as total,
            SUM(arithmos_mathitwn) as sold
            FROM savecontactform7_5 GROUP BY Ekpaideutiko_programma");
                foreach ($result as $print) {
                    ?>
                    <tbody>
                    <tr>
                        <th class="check-column" scope="row"><input type="checkbox"/></th>
                        <td class="column-columnname"><strong><?php echo $print->name; ?></strong></td>
                        <td class="column-columnname"><?php echo $print->total; ?></td>
                        <td <?php if ($print->sold >= 50 && $print->sold < 500): ?>
                            style="color:#088A08;"
                        <?php elseif ($print->sold >= 500 && $print->sold < 1000): ?>
                            style="color:#8A4B08;"
                        <?php elseif ($print->sold >= 1000): ?>
                            style="color:#FE2E2E; background-color:#F78181"
                        <?php endif; ?>>
                            <?php if (is_null($print->sold)) {
                                echo "0";
                            } else {
                                echo $print->sold;
                            } ?>/1000
                        </td>
                    </tr>
                    </tbody>
                <?php } ?>
            </table>
        </form>
    </div>
    <?php
}

?>
